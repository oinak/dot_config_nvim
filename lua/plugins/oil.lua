return {
  {
    'stevearc/oil.nvim',
    opts = {},
    lazy = false,
    keys = {
      { "-", vim.cmd.Oil, desc = "Open parent folder" },
    },
    -- Optional dependencies
    dependencies = { "nvim-tree/nvim-web-devicons" },
    config = function()
      require('oil').setup({
        default_file_explorer = true,
        delete_to_trash = true,
        skip_confirm_for_simple_edits = true,
        keymaps = {
          ["<C-h>"] = false, -- do not interfere with split navigation
          ["<C-l>"] = false, -- do not interfere with split navigation
          ["<M-h>"] = "actions.select_split"
          -- ["<C-p>"] = "actions.preview",
        },
        view_options = {
          show_hidden = true,
          natural_order = true,
        },
        win_options = { wrap = true },

        vim.keymap.set("n", "-", vim.cmd.Oil, { desc = "Open parent folder" })
      })
    end
  },

}
