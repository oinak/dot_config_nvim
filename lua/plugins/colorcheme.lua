return {
  {
    "savq/melange-nvim",
    lazy = false,
    priority = 1000,
    config = function()
      vim.opt.termguicolors = true
      -- vim.cmd.colorscheme 'melange'
      -- vim.opt.background = "light"
    end,
  },
  -- Or with configuration
  {
    'projekt0n/github-nvim-theme',
    name = 'github-theme',
    lazy = false,    -- make sure we load this during startup if it is your main colorscheme
    priority = 1000, -- make sure to load this before all the other start plugins
    config = function()
      require('github-theme').setup({})

      vim.opt.background = "light"
      vim.cmd('colorscheme github_light_high_contrast')
    end,
  },
}
