return {
  {
    "2KAbhishek/co-author.nvim", -- fuzzy search of coauthors from repo
    dependencies = { "stevearc/dressing.nvim" },
    cmd = "CoAuthor",
  },
}
