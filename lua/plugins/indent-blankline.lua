return {
  {
    "lukas-reineke/indent-blankline.nvim",
    main = "ibl",
    opts = {
      indent = {
        char = " ",
        smart_indent_cap = true,
        highlight = "Comment",
      },
      whitespace = {
        highlight = "Comment",
      },
      scope = {
        enabled = true,
        char = "│",
      }
    },
  },
}
