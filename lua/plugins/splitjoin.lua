return {
  {
    "AndrewRadev/splitjoin.vim", -- Split/Join ruby hashes, arglists, etc
    config = function()
      -- Splitjoin keymaps
      vim.keymap.set("n", "ss", vim.cmd.SplitjoinSplit, { desc = "[S]plitjoin [S]plit" })
      vim.keymap.set("n", "sj", vim.cmd.SplitjoinJoin, { desc = "[S]plitjoin [J]oin" })
    end,
  },
}
