return {
  {
    "AndrewRadev/switch.vim", -- Automate common substitutions
    config = function()
      -- remap default mapping to add description
      vim.keymap.set("n", "gs", vim.cmd.Switch, { desc = "[S]witch toggle" })
    end,
  },
}
