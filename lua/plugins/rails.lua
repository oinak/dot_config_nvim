return {
  {
    'tpope/vim-rails', -- Rails support
    config = function()
      -- managed by gutentags:
      -- vim.g.rails_ctags_arguments =
      -- "-f .tags --languages=ruby --exclude=.git --exclude=log --exclude=tmp --exclude=.bundle -R . &"
      vim.api.nvim_create_autocmd("FileType", {
        pattern = "eruby.yaml",
        command = "set filetype=yaml",
      })
    end
  },
}
