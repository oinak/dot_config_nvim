return {
  {
    'vim-ruby/vim-ruby',
    config = function() vim.cmd('runtime macros/matchit.vim') end,
  },
  {
    'kmdsbng/vim-ruby-eval', -- Eval ruby inline (for scraps)
    config = function()
      vim.keymap.set('n', '<leader>R', "<ESC>:RubyEval<CR>", { desc = '[R]ubyeval' })
      vim.keymap.set('n', 'gcr', "<ESC>A # =><ESC>:RubyEval<CR>", { desc = '[c]omment with [r]ubyeval' })
      vim.keymap.set('n', 'gcd', "<ESC>:s/\\s\\+#.*$//<CR>:noh<CR>", { desc = '[c]comment [d]elete' })
    end,
    dependencies = {
      'vim-ruby/vim-ruby',
    }
  },
  -- { 'tpope/vim-bundler' }, -- use bundler from vim

  -- make vim understand ruby blocks as motions
  { 'nelstrom/vim-textobj-rubyblock', dependencies = { 'kana/vim-textobj-user', } },
  { 'rhysd/vim-textobj-ruby',         dependencies = { 'kana/vim-textobj-user', } },
}
