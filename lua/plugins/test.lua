return {
  {
    'klen/nvim-test', -- run tests from nvim
    dependencies = { "nvim-treesitter/nvim-treesitter", },
    config = function()
      local plugin = require('nvim-test')

      plugin.setup({
        termOpts = {
          direction = "vertical", -- terminal's direction ("horizontal"|"vertical"|"float")
          width = 138,
          height = 37,
        },
      })

      require('nvim-test.runners.rspec'):setup({
        command = 'docker',
        args = { "compose", "exec", "web", "bundle", "exec", "rspec" },
        file_pattern = "\\v(spec_[^.]+|[^.]+_spec)\\.rb$",
        find_files = { "{name}_spec.rb" },
      })

      vim.keymap.set("n", "<leader>tH", function()
        require('nvim-test').setup({ termOpts = { direction = "horizontal", }, })
      end, { desc = '[T]est [H]orizontal' })
      vim.keymap.set("n", "<leader>tV", function()
        require('nvim-test').setup({ termOpts = { direction = "vertical", }, })
      end, { desc = '[T]est [V]ertical' })
      -- vim.keymap.set("n", "<leader>tF", function()
      -- end, { desc = '[T]est [F]loat' })


      local testWindow = function(cmd)
        if plugin.config.termOpts.direction == 'horizontal' then
          local winWidth = vim.api.nvim_win_get_width(0)
          plugin.setup({ termOpts = { direction = 'horizontal', width = (winWidth * 0.5) } })
        else
          local winHeight = vim.api.nvim_win_get_height(0)
          plugin.setup({ termOpts = { direction = 'vertical', height = (winHeight * 0.5) } })
        end

        return cmd
      end

      vim.keymap.set("n", "<leader>tf", testWindow(vim.cmd.TestFile), { desc = '[T]est [F]ile' })
      vim.keymap.set("n", "<leader>tn", testWindow(vim.cmd.TestNearest), { desc = '[T]est [N]earest' })
      vim.keymap.set("n", "<leader>ts", testWindow(vim.cmd.TestSuite), { desc = '[T]est [S]uite' })
      vim.keymap.set("n", "<leader>tl", testWindow(vim.cmd.TestLast), { desc = '[T]est [L]ast' })
      vim.keymap.set("n", "<leader>tv", testWindow(vim.cmd.TestVisit), { desc = '[T]est [V]isit' })

      -- vim.keymap.set("n", "<leader>tn", vim.cmd.TestNearest, { desc = '[T]est [N]earest' })
      -- vim.keymap.set("n", "<leader>tf", vim.cmd.TestFile, { desc = '[T]est [F]ile' })
      -- vim.keymap.set("n", "<leader>ts", vim.cmd.TestSuite, { desc = '[T]est [S]uite' })
      -- vim.keymap.set("n", "<leader>tl", vim.cmd.TestLast, { desc = '[T]est [L]ast' })
      -- vim.keymap.set("n", "<leader>tv", vim.cmd.TestVisit, { desc = '[T]est [V]isit' })
    end,
  },
}
