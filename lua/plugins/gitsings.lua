return {
  {
    "lewis6991/gitsigns.nvim", -- pure lua git decorations
    cmd = "Gitsigns",
    event = { "BufReadPre", "BufNewFile" },
    opts = {
      numhl = true,
      word_diff = false,
      current_line_blame = true,
      current_line_blame_opts = {
        virt_text = true,
        virt_text_pos = "right_align", -- 'eol' | 'overlay' | 'right_align'
        delay = 1000,
        ignore_whitespace = true,
      },
    }
  },
}
