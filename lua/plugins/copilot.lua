return {
  {
    "zbirenbaum/copilot.lua",
    cmd = "Copilot",
    event = "InsertEnter",
    config = function()
      require("copilot").setup({
        panel = { enabled = false, },
        suggestion = { enabled = false, },
      })
    end,
  },
  {
    "CopilotC-Nvim/CopilotChat.nvim",
    branch = "main",
    dependencies = {
      { "zbirenbaum/copilot.lua" }, -- or github/copilot.vim
      { "nvim-lua/plenary.nvim" },  -- for curl, log wrapper
    },
    build = "make tiktoken",        -- Only on MacOS or Linux
    config = function()
      local chat = require("CopilotChat")
      chat.setup({
        model = "gpt-4o", -- "claude-3.5-sonnet",
        context = "buffers",
      })
      -- Set keymaps after setup is complete
      vim.keymap.set({ "n", "v" }, "<leader>Co", vim.cmd.CopilotChatOpen, { desc = "Open CopilotChatOpen" })
      vim.keymap.set({ "n", "v" }, "<leader>Cc", vim.cmd.CopilotChatClose, { desc = "CopilotChatClose" })
      vim.keymap.set({ "n", "v" }, "<leader>Cf", vim.cmd.CopilotChatFix, { desc = "CopilotChatFix" })
      vim.keymap.set({ "n", "v" }, "<leader>Ct", vim.cmd.CopilotChatTest, { desc = "CopilotChatTest" })
      vim.keymap.set({ "n", "v" }, "<leader>Ca", function()
        -- Pick a prompt using vim.ui.select
        local actions = require("CopilotChat.actions")

        -- Pick prompt actions
        actions.pick(actions.prompt_actions({
          selection = require("CopilotChat.select").visual,
        }))
      end, {
        desc = "CopilotChat Actions",
      })
    end
  },
}
