local function cmp_setup()
  local cmp     = require("cmp")
  local lspkind = require("lspkind")
  local ls      = require("luasnip")
  -- vim.api.nvim_set_hl(0, "CmpItemKindCopilot")

  require("luasnip.loaders.from_vscode").load()
  ls.filetype_extend("ruby", { "rails" })

  -- initialize global var to false -> nvim-cmp turned off per default
  vim.g.cmptoggle = true
  vim.keymap.set(
    "n",
    "<leader><tab>",
    function()
      vim.g.cmptoggle = not vim.g.cmptoggle
      print("Completion set to:", vim.g.cmptoggle)
    end,
    { desc = "toggle nvim-cmp" }
  )
  -- see `:h completeopt` for details
  vim.opt.completeopt = { "menu", "menuone", "noselect", "noinsert", "preview" }

  -- Use Tab to jump to insertion points on a snippet
  vim.keymap.set({ "i", "s" }, "<tab>", function()
    if ls.expand_or_jumpable() then
      ls.expand_or_jump()
    end
  end, { silent = true })
  vim.keymap.set({ "i", "s" }, "<s-tab>", function()
    if ls.jumpable(-1) then
      ls.jump(-1)
    end
  end, { silent = true })

  cmp.setup({
    enabled = function()
      return vim.g.cmptoggle
    end,

    snippet = {
      expand = function(args)
        require("luasnip").lsp_expand(args.body) -- For `luasnip` users.
      end,
    },

    view = { entries = "custom", }, -- custom, native, wildmenu

    window = {
      completion = cmp.config.window.bordered(),
      documentation = cmp.config.window.bordered(),
    },

    preselect = cmp.PreselectMode.None,

    completion = { completeopt = "menu,menuone,noinsert,noselect", },

    mapping = cmp.mapping.preset.insert({
      ["<C-b>"] = cmp.mapping.scroll_docs(-4),
      ["<C-f>"] = cmp.mapping.scroll_docs(4),
      ["<C-y>"] = cmp.mapping.complete(),
      ['<CR>']  = cmp.mapping.confirm({ select = false }),
    }),

    sources = {
      { name = "nvim_lua", }, -- plugin excludes itself from non-lua buffers
      { name = "luasnip",  option = { use_show_condition = false } },
      { name = "nvim_lsp", },
      { name = "ctags", },
      -- 'group_index = 2' shows if there is no 'group_index = 1' available
      { name = "path", },
      { name = "buffer",   keyword_length = 3 },
      { name = "copilot", }
    },
    formatting = {
      format = lspkind.cmp_format({
        mode = "symbol_text", -- show symbol + text annotations
        maxwidth = 70, -- prevent the popup from showing more than provided characters (e.g 50 will not show more than 50 characters)
        ellipsis_char = "⋯", -- when popup menu exceed maxwidth, the truncated part would show ellipsis_char instead (must define maxwidth first)
        menu = {
          buffer    = "<buf",
          nvim_lsp  = "<lsp",
          nvim_lua  = "<api",
          path      = "<path",
          lua_snip  = "<snip",
          snippet   = "<snip",
          gh_issues = "<git",
          ctags     = "<tag",
          copilot   = "<ia",
          -- codeium   = "<ia"
        },
        symbol_map = {
          Copilot = "",
        }
      }),
    },
  })
  -- Set configuration for specific filetype.
  cmp.setup.filetype("gitcommit", {
    sources = cmp.config.sources({
      { name = "git" }, -- You can specify the `git` source if [you were installed it](https://github.com/petertriho/cmp-git).
    }, {
      { name = "buffer" },
    }),
  })
end

return {
  {
    "zbirenbaum/copilot-cmp",
    config = function()
      require("copilot_cmp").setup()
    end
  },
  -- Autocompletion
  {
    "hrsh7th/nvim-cmp",
    config = cmp_setup,
    dependencies = {
      --sources
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-cmdline",
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-nvim-lua",
      "delphinus/cmp-ctags",
      -- snippets
      "L3MON4D3/LuaSnip",
      "rafamadriz/friendly-snippets",
      "saadparwaiz1/cmp_luasnip",
      -- decorations
      "onsails/lspkind.nvim",
    }
  }
}
