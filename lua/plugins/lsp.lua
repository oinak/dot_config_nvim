return {
  {
    "williamboman/mason.nvim",
    cmd = "Mason",
    opts = {},
  },
  {
    "williamboman/mason-lspconfig.nvim",
    lazy = false,
    config = function()
      require("mason-lspconfig").setup({
        ensure_installed = {
          "eslint",
          "lua_ls",
          "rubocop",
          -- "solargraph",
          "ruby_lsp",
          "ts_ls",
          "gopls",
          "crystalline"
        },
        opts = {
          auto_install = true,
          inlay_hints = { enabled = true },
        }
      })
    end,
  },
  {
    "folke/neodev.nvim",
    opts = {},
    -- config = function()
    --   require("neodev").setup({}) -- must happen BEFORE lspconfig
    -- end,
  }, -- init.lua support
  {
    "neovim/nvim-lspconfig",
    event = { "BufReadPre", "BufnewFile" },
    config = function()
      require("neodev").setup({}) -- must happen BEFORE lspconfig
      local lspconfig = require("lspconfig")

      local capabilities = vim.lsp.protocol.make_client_capabilities()
      -- capabilities = vim.tbl_deep_extend("force", capabilities, require('blink.cmp').get_lsp_capabilities())
      capabilities = vim.tbl_deep_extend("force", capabilities, require("cmp_nvim_lsp").default_capabilities())

      lspconfig.lua_ls.setup({
        capabilities = capabilities,
        settings = {
          Lua = {
            diagnostics = {
              -- Get the language server to recognize the `vim` global
              globals = { "vim", "require", },
            },
            -- Do not send telemetry data containing a randomized but unique identifier
            telemetry = { enable = false, },
            hint = { enable = true }
          },
        },
      })

      lspconfig.solargraph.setup({
        capabilities = capabilities,
        init_options = { formatting = false },
        settings = {
          solargraph = {
            completion = true,
            hover = true,
            symbols = true,
            diagnostics = false
          },
        },
      })

      lspconfig.ruby_lsp.setup({
        -- cmd = { 'docker-compose', 'run', '--rm', 'web', 'bin/ruby-lsp' },
        -- cmd = { 'dx/ruby-lsp' },
        capabilities = capabilities,
        init_options = { formatter = "auto" },
        settings = {
          ruby_lsp = { inlayHint = { enableAll = true }, },
          diagnostics = {
            ignored = { "RequireNotFound" }
          },
        },
      })

      lspconfig["gopls"].setup({
        capabilities = capabilities,
      })

      lspconfig["rubocop"].setup({
        capabilities = capabilities,
        init_options = { formatting = true },
        settings = {
          rubocop = {
            displayCopNames = true,
            diagnostics = { ignored = "RequireNotFound" },
          },
        },
      })

      lspconfig["crystalline"].setup({
        capabilities = capabilities,
        init_options = { formatting = true },
      })

      local RangeFormatting = function()
        local start_row, _ = unpack(vim.api.nvim_buf_get_mark(0, "<"))
        local end_row, _ = unpack(vim.api.nvim_buf_get_mark(0, ">"))
        vim.lsp.buf.format({
          range = {
            ["start"] = { start_row, 0 },
            ["end"] = { end_row, 0 },
          },
          async = true,
        })
      end

      -- Inlay parameter hints
      if vim.lsp.inlay_hint then
        vim.keymap.set(
          "n",
          "<leader>H",
          function()
            local change = not vim.lsp.inlay_hint.is_enabled(vim.lsp.inlay_hint.enable.Filter)
            vim.lsp.inlay_hint.enable(change)
          end,
          { desc = "Toggle inlay hints" }
        )
      end

      -- Use LspAttach autocommand to only map the following keys
      -- after the language server attaches to the current buffer
      vim.api.nvim_create_autocmd("LspAttach", {
        group = vim.api.nvim_create_augroup("UserLspConfig", {}),
        callback = function(ev)
          -- Enable completion triggered by <c-x><c-o>
          vim.bo[ev.buf].omnifunc = "v:lua.vim.lsp.omnifunc"

          local keymap = function(mode, key, action, desc)
            vim.keymap.set(mode, key, action, { buffer = ev.buf, remap = false, desc = desc })
          end

          keymap("n", "gd", vim.lsp.buf.definition, "go to definition (LSP)")
          keymap("n", "gD", vim.lsp.buf.declaration, "go to declaration (LSP)")
          keymap("n", "gr", vim.lsp.buf.references, "go to [r]eferences (LSP)")
          keymap("n", "gi", vim.lsp.buf.implementation, "goto implementation (LSP)")
          keymap("n", "gK", vim.lsp.buf.hover, "show info (LSP)")     -- K is now default for show_info
          keymap("n", "R", vim.lsp.buf.rename, "rename symbol (LSP)") -- default is <F2>
          keymap("n", "g=", vim.lsp.buf.format, "reformat (LSP)")
          keymap("v", "g=", RangeFormatting, "reformat (LSP)")
          keymap("n", "gl", vim.lsp.diagnostic.get_line_diagnostics, "line diagnostic (LSP)")
          -- keymap("n", "[d", vim.lsp.diagnostic.goto_prev, "prev line diagnostic (LSP)")
          -- keymap("n", "]d", vim.lsp.diagnostic.goto_next , "nexr line diagnostic (LSP)")
          keymap("n", "<C-k>", vim.lsp.buf.signature_help, "signature help (LSP)")
          keymap("n", "<space>wa", vim.lsp.buf.add_workspace_folder, "add workspace folder (LSP)")
          keymap("n", "<space>wr", vim.lsp.buf.remove_workspace_folder, "remove workspace folder (LSP)")
          keymap("n", "<space>wl", function()
            print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
          end, "(LSP) list workspace folders")
          keymap("n", "<space>D", vim.lsp.buf.type_definition, "type definition (LSP)")
          keymap({ "n", "v" }, "<space>ca", function()
            vim.lsp.buf.code_action({ apply = true })
          end, "code action (LSP)")
        end,
      })
    end,
  },
  {
    "folke/trouble.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    opts = {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
    },
    config = function()
      vim.keymap.set("n", "<leader>xx", function() require("trouble").toggle("diagnostics") end)
      vim.keymap.set("n", "<leader>xw", function() require("trouble").toggle("workspace_diagnostics") end)
      vim.keymap.set("n", "<leader>xd", function() require("trouble").toggle("document_diagnostics") end)
      vim.keymap.set("n", "<leader>xq", function() require("trouble").toggle("quickfix") end)
      vim.keymap.set("n", "<leader>xl", function() require("trouble").toggle("loclist") end)
      vim.keymap.set("n", "gR", function() require("trouble").toggle("lsp_references") end)
    end
  },
  {
    -- Autoformat on save,
    "stevearc/conform.nvim",
    config = function()
      require("conform").setup({
        format_on_save = {
          -- These options will be passed to conform.format()
          timeout_ms = 500,
          lsp_fallback = true,
        },
      })
    end,
  },
  {
    "WhoIsSethDaniel/toggle-lsp-diagnostics.nvim",
    config = function()
      require("toggle_lsp_diagnostics").init()
      vim.keymap.set("n", "<leader>x", ":ToggleDiag<cr>", { desc = "Toggle LSP diagnostics (warnings)" })
    end,
  },
  -- Signature
  {
    "ray-x/lsp_signature.nvim",
    config = function()
      require("lsp_signature").setup()
      vim.keymap.set("n", "<C-k>", function()
        require("lsp_signature").toggle_float_win()
      end, { silent = true, noremap = true, desc = "toggle signature" })
    end,
  },
}
