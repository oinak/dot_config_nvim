return {
  {
    'vim-crystal/vim-crystal',
    config = function()
      vim.g.crystal_auto_format = 1
      vim.g.crystal_define_mappings = 0
    end,
  },
}
