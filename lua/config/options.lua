--
-- OPTIONS
--

-- Set <space> as the leader key
-- See `:help mapleader`
--  NOTE: Must happen before plugins are loaded (otherwise wrong leader will be used)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Set to true if you have a Nerd Font installed and selected in the terminal
vim.g.have_nerd_font = true

-- Better editor UI
vim.opt.number = true
vim.opt.relativenumber = false
-- vim.opt.numberwidth = 3
vim.opt.signcolumn = 'yes'
vim.opt.cursorline = false
vim.opt.colorcolumn = '80,120'

vim.opt.mouse = 'a' -- Enable mouse mode, can be useful for resizing splits for example!

-- Don't show the mode, since it's already in the status line
vim.opt.showmode = false

-- Sync clipboard between OS and Neovim.
--  Schedule the setting after `UiEnter` because it can increase startup-time.
--  Remove this option if you want your OS clipboard to remain independent.
--  See `:help 'clipboard'`
vim.schedule(function()
  vim.opt.clipboard = 'unnamedplus'
end)

vim.opt.breakindent = true -- Enable break indent

vim.opt.ignorecase = true  -- Case-insensitive searching UNLESS \C or one or more capital letters in the search term
vim.opt.smartcase = true

vim.opt.signcolumn = 'yes' -- Keep signcolumn on by default
vim.opt.updatetime = 50    -- Decrease update time
vim.opt.timeoutlen = 300   -- Decrease mapped sequence wait time Displays which-key popup sooner
vim.opt.splitright = true  -- Configure how new splits should be opened
vim.opt.splitbelow = true

vim.opt.list = false
-- vim.opt.listchars = { tab = '» ', trail = '·', nbsp = '␣' }
vim.opt.listchars = 'trail:·,nbsp:◇,tab:→ ,extends:▸,precedes:◂,eol:⁋'
-- o.listchars = 'eol:¬,space:·,lead: ,trail:·,nbsp:◇,tab:→-,extends:▸,precedes:◂,multispace:···⬝,leadmultispace:│   ,'
-- o.formatoptions = 'qrn1'

vim.opt.inccommand = 'split' -- Preview substitutions live, as you type!
vim.opt.cursorline = false   -- Show which line your cursor is on
vim.opt.scrolloff = 8        -- Minimal number of screen lines to keep above and below the cursor.
vim.opt.sidescrolloff = 8    -- Minimal number of screen columns to keep left and right of the cursor.

-- sensible defaults from https://www.youtube.com/watch?v=J9yqSdvAKXY
vim.opt.backspace = '2'
vim.opt.showcmd = true
vim.opt.autowrite = true -- save the file before leaving if changed
vim.opt.autoread = true  -- auto load file changes occured outside vim
-- use spaces for tabs and whatnot
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.shiftround = true -- round indent to sw compatible
vim.opt.expandtab = true

vim.opt.expandtab = true
-- o.smarttab = true
vim.opt.cindent = true
-- o.autoindent = true
vim.opt.wrap = false     -- using wrapping-paper to show it in virtual text.
vim.opt.textwidth = 300
vim.opt.softtabstop = -1 -- If negative, shiftwidth value is used

-- Undo and backup options
vim.opt.backup = false
vim.opt.writebackup = false
vim.opt.undofile = true
vim.opt.swapfile = false
vim.cmd('set path+=**') -- vimd find searches into subdurectories

vim.opt.directory = vim.fn.stdpath('data') .. 'tmp'
vim.opt.undodir = vim.fn.stdpath('data') .. 'undo'

-- Remember 1000 items in commandline history
vim.opt.history = 1000

-- mouse mode
vim.opt.mouse = 'a'

if vim.fn.has('macunix') == 1 then
  vim.opt.guifont = "FiraCode Nerd Font Light:h19"
end

if vim.fn.has('linux') == 1 then
  vim.opt.guifont = "FiraCode Nerd Font Mono Light:h11"
end

if vim.g.neovide then
  -- Put anything you want to happen only in Neovide here
  vim.opt.guifont = "FiraCode Nerd Font Mono:h15"
  vim.keymap.set({ "n", "v" }, "<C-=>", ":lua vim.g.neovide_scale_factor = vim.g.neovide_scale_factor + 0.1<CR>")
  vim.keymap.set({ "n", "v" }, "<C-->", ":lua vim.g.neovide_scale_factor = vim.g.neovide_scale_factor - 0.1<CR>")
  vim.keymap.set({ "n", "v" }, "<C-0>", ":lua vim.g.neovide_scale_factor = 1<CR>")
  vim.g.neovide_scroll_animation_length = 0.05
  vim.g.neovide_floating_shadow = false
  vim.g.neovide_transparency = 1
end

-- WIP Global Statusline + slick separator
vim.opt.laststatus = 3
vim.api.nvim_set_hl(0, 'WinSeparator', { bg = 'None' })
-- vim.opt.showtabline = 2 -- overriden by winbar

-- vim.highlight.create('WinSeparator', { guibg = none }, false)
vim.g['netrw_banner'] = 0
vim.g['netrw_liststyle'] = 3
vim.g['netrw_winsize'] = 25

vim.cmd("ab coaby Co-authored-by: name <@sequra.es>")
vim.cmd("ab coatom Co-authored-by: tomtomecek-es <tomas.tomecek@sequra.es>")
vim.cmd("ab coacar Co-authored-by: carlosiniesta <carlos.iniesta@sequra.es>")
vim.cmd("ab coaali Co-authored-by: ahmad-aliii <ahmad.ali@sequra.es>")
vim.cmd("ab coaanc Co-authored-by: aancoratsequra <ancor.cruz@sequra.es>")
vim.cmd("ab coacal Co-authored-by: nosk14 <carlos.lucas@sequra.es>")
vim.cmd("ab coajul Co-authored-by: julienmn-work <julien.maisonneuve@sequra.es>")
vim.cmd("ab coagab Co-authored-by: gramos4 <gabriel.ramos@sequra.es>")

vim.opt.termguicolors = true

vim.api.nvim_create_autocmd({ "BufNewFile" }, {
  pattern = { "*.sh" },
  callback = function(ev) -- pass ev if needed
    vim.cmd("0r ~/.config/nvim/skeleton/bash.sh")
  end
})
